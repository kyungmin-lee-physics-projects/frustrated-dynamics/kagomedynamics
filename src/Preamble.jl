import Logging
import Dates
import Formatting

function get_rss()
    result = Vector{Csize_t}(undef, 1)
    ccall(:uv_resident_set_memory, Cint, (Ptr{Csize_t},), Ref(result,1))
    return result[1]
end

BN(x::Integer) = Formatting.format(Int(x), commas=true)

function my_metafmt(level, _module, group, id, file, line)
    color = Logging.default_logcolor(level)
    date_string = Dates.format(Dates.now(), "yyyy-mm-ddTHH:MM:SS.sss")
    prefix = "$(date_string) | $(level) | $(id) | Thread: $(Threads.threadid()) | RSS: $(BN(get_rss())) |"
    suffix = ""
    Logging.Info <= level < Logging.Warn && return color, prefix, suffix
    _module !== nothing && (suffix *= "$(_module)")
    if file !== nothing
        _module !== nothing && (suffix *= " ")
        suffix *= Base.contractuser(file)
        if line !== nothing
            suffix *= ":$(isa(line, UnitRange) ? "$(first(line))-$(last(line))" : line)"
        end
    end
    !isempty(suffix) && (suffix = "@ " * suffix)
    return color, prefix, suffix
end

global loglock = ReentrantLock()
function logmsg(msg::AbstractString...)
    text = join("$x" for x in msg)
    global loglock
    lock(loglock)
    @info text
    flush(stdout)
    unlock(loglock)
end

macro mylogmsg(msg)
    return quote
        global loglock
        lock(loglock)
        @info $(esc(msg))
        flush(stdout)
        unlock(loglock)
    end
end

