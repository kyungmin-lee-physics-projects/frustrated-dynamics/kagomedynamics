using LinearAlgebra
using DataFrames
using CSV
using LatticeTools
using QuantumHamiltonian
using Formatting

struct FunctionVector{S}<:AbstractVector{S}
    n::Integer
    f::Function
end

Base.getindex(fv::FunctionVector{S}, i::Integer) where S = fv.f(i)
Base.size(fv::FunctionVector) = (fv.n,)


function symmetry_reduce_parallel_cached!(
    out::AbstractVector{<:Number},
    basis_mapping_index::AbstractVector{<:Integer},
    basis_mapping_amplitude::AbstractVector{<:Number},
    basis_mapping_reverse_index::AbstractMatrix{<:Integer},
    large_vector::AbstractVector{<:Number}
)
    if length(basis_mapping_index) != length(basis_mapping_amplitude)
        throw(DimensionMismatch("Dimension of basis_mapping"))
    elseif size(basis_mapping_reverse_index, 2) > length(basis_mapping_index)
        throw(ArgumentError("basis mapping reverse index too large"))
    elseif size(basis_mapping_reverse_index, 2) != length(out)
        throw(DimensionMismatch("Dimension of the output vector should match the smaller representation"))
    elseif length(large_vector) != length(basis_mapping_index)
        throw(DimensionMismatch("Dimension of the input vector should match the larger representation"))
    end
    # (i_p | i_r | ampl) indicates : U_(p, r) = ampl
    n_p = length(basis_mapping_index)
    n_g, n_r = size(basis_mapping_reverse_index)
    Threads.@threads for i_r in 1:n_r
        for i_g in 1:n_g
            i_p = basis_mapping_reverse_index[i_g, i_r]
            i_p <= 0 && break
            @inbounds ampl = basis_mapping_amplitude[i_p]
            @inbounds val = conj(ampl) * large_vector[i_p]
            @inbounds out[i_r] += val
        end
    end
    return out
end



function precompute_sectors(
    shape::AbstractMatrix{<:Integer},
    coloring::AbstractVector{<:Integer},
    output_dirpath::AbstractString,
    target_spin_z_list::AbstractVector{<:Real}=Float64[],
)
    @mylogmsg "lattice shape: $shape"

    @mylogmsg("Will save results to $output_dirpath")
    kagome = make_kagome_lattice(shape)
    lattice = kagome.lattice
    ssymbed = kagome.space_symmetry_embedding

    n_sites = numsite(kagome.lattice.supercell)
    @mylogmsg("Number of sites: $n_sites")

    if n_sites <= 64
        BR = UInt64
    elseif n_sites <= 128
        BR = UInt128
    elseif n_sites <= 256
        BR = UInt256
    elseif n_sites <= 512
        BR = UInt512
    elseif n_sites <= 1024
        BR = UInt1024
    else
        error("Too many sites to be reprented with unsigned integer")
    end

    hs, pauli = QuantumHamiltonian.Toolkit.spin_half_system(n_sites, BR)

    @mylogmsg "Constructing initial state"
    local_phase_types = [
        [0,0],
        [-1,1],
        [-2,2],
    ]

    @mylogmsg "Quantum numbers: $(quantum_number_sectors(hs))"
    qns = quantum_number_sectors(hs)

    norm_factor = sqrt(0.5)^(n_sites)
    phase_amplitudes = [cis((2π/6) * (i-1)) * norm_factor for i in 1:6]

    if isempty(target_spin_z_list)
        target_qn_list = [x for (x,) in qns]
    else
        target_qn_list = intersect([x for (x,) in qns], [round(Int, 2*Sz) for Sz in target_spin_z_list])
    end

    @mylogmsg "Target quantum numbers: $(target_qn_list)"

    for qn in target_qn_list
        Sz = qn/2
        output_filepath = joinpath(output_dirpath, format("sectors_Sz={:.1f}.csv", Sz))
        if isfile(output_filepath)
            @mylogmsg "File $output_filepath exists. Continuing"
            continue
        end

        spin_z_sector = []
        @mylogmsg "Creating Hilbert space sector Sz = $Sz"
        hss = HilbertSpaceSector(hs, qn)
        hssr = represent_array(hss)

        @mylogmsg "Constructing state in Sz sector (dimension: $(dimension(hssr)))"
        psi_sz_sector = let
            phase_sz_sector = zeros(Int8, dimension(hssr))
            Threads.@threads for ivec in 1:dimension(hssr)
                bvec = hssr.basis_list[ivec]
                for (isite, istate) in enumerate(QuantumHamiltonian.extract(hs, bvec).I)
                    phase_sz_sector[ivec] = mod(phase_sz_sector[ivec] + local_phase_types[coloring[isite]][istate], Int8(6))
                end
            end
            phase_sz_sector .+= 1
            FunctionVector{ComplexF64}(dimension(hssr), ivec -> phase_amplitudes[ phase_sz_sector[ivec] ])
        end

        # function getphi(ivec::Integer)
        #     bvec = hssr.basis_list[ivec]
        #     phase = 0
        #     for (isite, istate) in enumerate(QuantumHamiltonian.extract(hs, bvec).I)
        #         phase += local_phase_types[coloring[isite]][istate]
        #     end
        #     phase = mod(phase, 6)
        #     return cis((2π/6) * phase) * norm_factor
        # end
        # psi_sz_sector = FunctionVector{ComplexF64}(dimension(hssr), getphi)

        for ssic in get_irrep_components(ssymbed)
            k = fractional_momentum(ssymbed, ssic.normal.irrep_index)
            rhssr = symmetry_reduce(hssr, ssic)
            hilbert_space_dimension = dimension(rhssr)
            @mylogmsg "Space sector ($(ssic.normal.irrep_index), $(ssic.rest.irrep_index), $(ssic.rest.irrep_component)) has dimension $(hilbert_space_dimension)"
            if dimension(rhssr) == 0
                overlap_squared = 0.0
            else
                n_p = dimension(rhssr.parent)
                n_r = dimension(rhssr)
                n_g = group_order(ssic)
                @mylogmsg "Constructing reverse map for threading"

                basis_mapping_reverse_index_size = zeros(Int, n_r)
                basis_mapping_reverse_index = Matrix{Int}(undef, (n_g, n_r))
                fill!(basis_mapping_reverse_index, -1)

                for i_p in 1:n_p
                    i_r = rhssr.basis_mapping_index[i_p]
                    i_r <= 0 && continue
                    basis_mapping_reverse_index_size[i_r] += 1
                    basis_mapping_reverse_index[basis_mapping_reverse_index_size[i_r], i_r] = i_p
                end
                temporary_out_vector = zeros(ComplexF64, dimension(rhssr))
                @mylogmsg "Symmetry reducing sector (cached)"
                symmetry_reduce_parallel_cached!(temporary_out_vector, rhssr.basis_mapping_index, rhssr.basis_mapping_amplitude, basis_mapping_reverse_index, psi_sz_sector)
                overlap_squared = sum(abs2.(temporary_out_vector))
                if overlap_squared < eps(Float64)
                    overlap_squared = 0.0
                end
                GC.gc()
            end
            @mylogmsg "‖P|ψ⟩‖² = $overlap_squared"
            sector_item = (
                spin_z=Sz,
                tii=ssic.normal.irrep_index,
                pii=ssic.rest.irrep_index,
                pic=ssic.rest.irrep_component,
                dimension=dimension(rhssr),
                overlap_squared=overlap_squared,
            )
            push!(spin_z_sector, sector_item)
        end
        df = DataFrame(spin_z_sector)
        !isdir(output_dirpath) && mkpath(output_dirpath)
        CSV.write(output_filepath, df)
    end # for qn

end
