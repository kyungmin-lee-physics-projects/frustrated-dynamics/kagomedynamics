using DrWatson
@quickactivate "KagomeDynamics"

using Logging
using LinearAlgebra
using Printf
using DataStructures

using ArgParse

using LatticeTools
using QuantumHamiltonian
using KagomeDynamics
using KagomeDynamics: my_metafmt

import KagomeDynamics.precompute_sectors

function parse_commandline()
    s = ArgParseSettings()
    @add_arg_table! s begin
        "--shape"
            arg_type = Int
            nargs = 4
            required = true
        "--coloring"
            arg_type = String
            required = true
            range_tester = x -> all(y->y in "123_", x)
        "--force", "-f"
            help = "force run (overwrite)"
            action = :store_true
        "--Sz"
            arg_type = Float64
            nargs = '+'
            default = []
            range_tester = x -> round(Int, 2*x) == 2*x
        "--debug", "-d"
            help = "debug"
            action = :store_true
    end
    return parse_args(s)
end


function main()
    parsed_args = parse_commandline()
    if parsed_args["debug"]
        logger = ConsoleLogger(stdout, Logging.Debug; meta_formatter=my_metafmt)
        global_logger(logger)
    else
        logger = ConsoleLogger(stdout, Logging.Info; meta_formatter=my_metafmt)
        global_logger(logger)
    end
    ss = parsed_args["shape"]
    shape = [ss[1] ss[3]; ss[2] ss[4]]
    coloring = [parse(Int, x) for x in replace(parsed_args["coloring"], "_"=> "")]
    if length(coloring) != round(Int, det(shape)*3)
        @error "Length of initialstate should match number of sites"
        exit(1)
    end

    n11 = shape[1,1]
    n12 = shape[1,2]
    n21 = shape[2,1]
    n22 = shape[2,2]

    shape_str = "($n11,$n21)x($n12,$n22)"
    parameter_filename = Dict{Symbol, Any}(
        :shape=>shape_str,
        :coloring=>join(string.(coloring)),
    )
    output_dirname = savename("dynamics", parameter_filename, "dynamics")
    output_dirpath = datadir("kagome", shape_str, output_dirname, "sector_overlap")

    spinz_list = parsed_args["Sz"]
    precompute_sectors(shape, coloring, output_dirpath, spinz_list)
end


main()
