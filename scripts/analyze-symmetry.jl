using DrWatson
@quickactivate "KagomeDynamics"

using Logging
using ArgParse
using LinearAlgebra
using Printf

using DataStructures

using LatticeTools
using QuantumHamiltonian
using KagomeDynamics

using JSON

function analyze_symmetry(
    shape::AbstractMatrix{<:Integer},
)
    shape_str = "($(shape[1,1]),$(shape[2,1])x($(shape[1,2]),$(shape[2,2]))"

    kagome = make_kagome_lattice(shape)
    lattice = kagome.lattice
    ssymbed = kagome.space_symmetry_embedding

    n_sites = numsite(kagome.lattice.supercell)

    if n_sites <= 64
        BR = UInt64
    elseif n_sites <= 128
        BR = UInt128
    elseif n_sites <= 256
        BR = UInt256
    elseif n_sites <= 512
        BR = UInt512
    elseif n_sites <= 1024
        BR = UInt1024
    else
        error("Too many sites to be reprented with unsigned integer")
    end

    hs, pauli = QuantumHamiltonian.Toolkit.spin_half_system(n_sites, BR)
    qns = quantum_number_sectors(hs)
    Szs = [qn/2 for (qn,) in qns]
    space_sectors = []

    for ssic in get_irrep_components(ssymbed)
        k = fractional_momentum(ssymbed, ssic.normal.irrep_index)
        push!(space_sectors,
            OrderedDict(
                "translation_symmetry_irrep_index" => ssic.normal.irrep_index,
                "momentum_in_twopi" => lattice.unitcell.reducedreciprocallatticevectors * k,
                "little_point_symmetry" => symmetry(ssic.rest.symmetry).hermann_mauguin,
                "point_symmetry_irrep_index" => ssic.rest.irrep_index,
                "point_symmetry_irrep_component" => ssic.rest.irrep_component,
            )
        )
    end

    output_data = OrderedDict(
        "SpinSectors" => Szs,
        "SpaceSectors" => space_sectors,
    )
    open(datadir("$shape_str.sectors"), "w") do io
        write(io, JSON.json(output_data))
    end
end


function parse_commandline()
    s = ArgParseSettings()
    @add_arg_table! s begin
        "--shape"
            arg_type = Int
            nargs = 4
            required = true
    end
    return parse_args(s)
end


# run in the background
function runbg(f)
    tid_main = Threads.threadid()
    simplelock = Threads.SpinLock()
    finished = Threads.Atomic{Bool}(false)
    Threads.@threads for i in 1:Threads.nthreads()
        if i != tid_main && trylock(simplelock) && !finished[]
            finished[] = true
            @async f()
            unlock(simplelock)
        end
    end
end

function main()
    parsed_args = parse_commandline()
    ss = parsed_args["shape"]
    shape = [ss[1] ss[3]; ss[2] ss[4]]
    analyze_symmetry(shape)
end

main()
