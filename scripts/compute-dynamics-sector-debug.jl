using DrWatson
@quickactivate "KagomeDynamics"

using Logging
using ArgParse
using LinearAlgebra
using SparseArrays
using Printf

using DataStructures

using CodecXz
using MsgPack

using Arpack
using PyCall
const npl = pyimport("numpy.linalg")
const spl = pyimport("scipy.linalg")
const sps = pyimport("scipy.sparse")
const spsl = pyimport("scipy.sparse.linalg")

using LatticeTools
using QuantumHamiltonian
using KagomeDynamics
using KagomeDynamics: my_metafmt
using SimpleLinear

using DataFrames
using CSV


# H⋅U = E⋅U
# U†⋅H⋅U = e
# V†⋅H⋅V = h = v⋅e⋅v† = v⋅U†⋅H⋅U⋅v†
function reeigen(op::Hermitian{T, <:AbstractMatrix{T}}, basis::AbstractMatrix{S}) where {T, S}
    @boundscheck if LinearAlgebra.checksquare(op) != size(basis, 1)
        throw(DimensionMismatch("op has dimensions $(size(op)), and basis has dimensions $(size(basis))"))
    end
    op_prime = adjoint(basis) * (op * basis)
    eigen_prime = eigen!(Hermitian(op_prime))
    return Eigen(eigen_prime.values, basis * adjoint(eigen_prime.vectors))
end



function compute_dynamics(
    Jx::Real, Jy::Real, Jz ::Real, J1::Real, J2::Real,
    shape::AbstractMatrix{<:Integer},
    Szs::AbstractVector{<:Real},
    coloring::AbstractVector{<:Integer},
    max_dense::Integer, nev::Integer,
    force::Bool,
)

    @mylogmsg "lattice shape: $shape"
    @mylogmsg "Jx: $Jx"
    @mylogmsg "Jy: $Jy"
    @mylogmsg "Jz: $Jz"
    @mylogmsg "J1: $J1"
    @mylogmsg "J2: $J2"
    @mylogmsg "Sz: $Szs"

    n11 = shape[1,1]
    n12 = shape[1,2]
    n21 = shape[2,1]
    n22 = shape[2,2]

    kagome = make_kagome_lattice(shape)
    lattice = kagome.lattice
    ssymbed = kagome.space_symmetry_embedding

    n_sites = numsite(kagome.lattice.supercell)
    @mylogmsg("Number of sites: $n_sites")


    if n_sites <= 64
        BR = UInt64
    elseif n_sites <= 128
        BR = UInt128
    elseif n_sites <= 256
        BR = UInt256
    elseif n_sites <= 512
        BR = UInt512
    elseif n_sites <= 1024
        BR = UInt1024
    else
        error("Too many sites to be reprented with unsigned integer")
    end

    hs, pauli = QuantumHamiltonian.Toolkit.spin_half_system(n_sites, BR)
    identity_op = PureOperator{Bool, BR}(LinearAlgebra.I)

    @mylogmsg("Generating spin operators")
    sx = 0.5 * sum(pauli(i, :x) for i in 1:n_sites)
    sy = 0.5 * sum(pauli(i, :y) for i in 1:n_sites)
    sz = 0.5 * sum(pauli(i, :z) for i in 1:n_sites)
    spin_squared = simplify(sx*sx + sy*sy + sz*sz)

    @mylogmsg("Generating Hamiltonian")
    jx, jy, jz = NullOperator(), NullOperator(), NullOperator()
    for (tri, sgn) in kagome.nearest_neighbor_triangles
        for ((i, j), R) in tri
            jx += pauli(i, :x) * pauli(j, :x)
            jy += pauli(i, :y) * pauli(j, :y)
            jz += pauli(i, :z) * pauli(j, :z)
        end
    end
    j1 = simplify(Jx*jx + Jy*jy + Jz*jz) * 0.25

    jx2, jy2, jz2 = NullOperator(), NullOperator(), NullOperator()
    for (tri, sgn) in kagome.next_nearest_neighbor_triangles
        for ((i, j), R) in tri
            jx2 += pauli(i, :x) * pauli(j, :x)
            jy2 += pauli(i, :y) * pauli(j, :y)
            jz2 += pauli(i, :z) * pauli(j, :z)
        end
    end
    j2 = simplify(Jx*jx2 + Jy*jy2 + Jz*jz2) * 0.25
    hamiltonian = simplify(j1*J1 + j2*J2)

    @mylogmsg("Generating initial state")
    sqrt_half = sqrt(0.5)
    ω = cis(2*π/3)
    local_state_types = [
        ComplexF64[1.0, 1.0] * sqrt_half,
        ComplexF64[1.0, ω] * sqrt_half,
        ComplexF64[1.0, conj(ω)] * sqrt_half
    ]

    local_states = ((local_state_types[i] for i in coloring)...,)
    #initial_state = QuantumHamiltonian.Toolkit.product_state(hs, local_states)
    #@mylogmsg("norm = $(norm(initial_state))")

    @mylogmsg("Quantum numbers: $(quantum_number_sectors(hs))")
    qns = isempty(Szs) ? quantum_number_sectors(hs) : [(round(Int, x*2), ) for x in Szs]


    all_eigenvalue_list = Float64[]
    all_spinz_list = Float64[]
    all_overlap_list = ComplexF64[]
    all_tii_list = Int[]
    all_pii_list = Int[]
    all_pic_list = Int[]
    all_matrix_type_list = String[]

    @mylogmsg("Target quantum numbers: $qns")
    for (qn,) in qns
        Sz = qn/2

        Sz_str = @sprintf "%.1f" Sz
        overlap_filepath = datadir(
            "kagome",
            "($n11,$n21)x($n12,$n22)",
            "dynamics_coloring=$(join(string.(coloring)))_shape=($n11,$n21)x($n12,$n22).dynamics",
            "sector_overlap",
            "sectors_Sz=$(Sz_str).csv")
        overlap_database = CSV.read(overlap_filepath, DataFrame)

        tol = Base.rtoldefault(Float64)
        if sum(overlap_database[overlap_database.spin_z .== Sz, :overlap_squared]) < tol
            @mylogmsg("Sz=$Sz: small overlap ($(sum(overlap_database[overlap_database.spin_z .== Sz, :overlap_squared])) < $(tol)). Skipping")
            continue
        else
            @mylogmsg("Sz=$Sz: enough overlap ($(sum(overlap_database[overlap_database.spin_z .== Sz, :overlap_squared])) ≥ $(tol)). Computing.")
        end

        @mylogmsg("Creating Hilbert space sector Sz = $Sz")
        hss = HilbertSpaceSector(hs, qn)
        hssr = represent_dict(hss)

        #(psi_sz_sector, _, _) = represent(hssr, initial_state)
        psi_sz_sector = [
            prod(
                local_state_types[coloring[isite]][istate]
                    for (isite, istate) in enumerate(QuantumHamiltonian.extract(hs, bvec).I)
            )
                for bvec in hssr.basis_list
        ]

        for ssic in get_irrep_components(ssymbed)
            tii = ssic.normal.irrep_index
            pii = ssic.rest.irrep_index
            pic = ssic.rest.irrep_component
            overlap_squared_sum = sum(overlap_database[
                (overlap_database.spin_z .== Sz) .&
                (overlap_database.tii .== tii) .&
                (overlap_database.pii .== pii) .&
                (overlap_database.pic .== pic),
                :overlap_squared])

            if overlap_squared_sum < tol
                @mylogmsg("Sz=$Sz, tii=$tii, pii=$pii, pic=$pic: small overlap ($overlap_squared_sum < $tol). Skipping.")
                continue
            else
                @mylogmsg("Sz=$Sz, tii=$tii, pii=$pii, pic=$pic: enough overlap ($overlap_squared_sum ≥ $tol). Computing.")
            end

            parameter_filename = Dict{Symbol, Any}(
                :shape=>"($n11,$n21)x($n12,$n22)",
                :Jx=>@sprintf("%.3f", Jx),
                :Jy=>@sprintf("%.3f", Jy),
                :Jz=>@sprintf("%.3f", Jz),
                :J1=>@sprintf("%.3f", J1),
                :J2=>@sprintf("%.3f", J2),
                :Sz=>@sprintf("%.1f", Sz),
                :tii=>tii,
                :pii=>pii,
                :pic=>pic,
                :coloring=>join(string.(coloring)),
            )
            output_filename = savename("dynamics", parameter_filename, "msgpack.xz")
            output_filepath = datadir("kagome", "($n11,$n21)x($n12,$n22)", output_filename)
            if ispath(output_filepath)
                @mylogmsg("File $output_filepath exists.")
                if force
                    @mylogmsg("Overwriting.")
                else
                    @mylogmsg("Skipping.")
                    continue
                end
            end
            @mylogmsg("Will save results to $output_filepath")


            k = fractional_momentum(ssymbed, ssic.normal.irrep_index)
            rhssr = symmetry_reduce(hssr, ssic)
            hilbert_space_dimension = dimension(rhssr)
            @mylogmsg("  - trans symmetry irrep index : $(ssic.normal.irrep_index)")
            @mylogmsg("    momentum (in units of 2π)  : $(lattice.unitcell.reducedreciprocallatticevectors * k)")
            @mylogmsg("    little point symmetry      : $(symmetry(ssic.rest.symmetry).hermann_mauguin)")
            @mylogmsg("    point symmetry irrep index : $(ssic.rest.irrep_index)")
            @mylogmsg("    point symmetry irrep compo : $(ssic.rest.irrep_component)")
            @mylogmsg("    Hilbert space dimension    : $(dimension(rhssr))")

            hilbert_space_dimension == 0 && continue

            psi_small = symmetry_reduce(rhssr, psi_sz_sector)
            if norm(psi_small) < Base.rtoldefault(Float64)
                @mylogmsg("Initial state has $(norm(psi_small)) overlap with this sector. Skipping.")
                continue
            end

            hamiltonian_rep = represent(rhssr, hamiltonian)
            # spin_squared_rep = represent(rhssr, spin_squared)

            if hilbert_space_dimension > max_dense
                matrix_type = "sparse"
            else
                matrix_type = "dense"
            end
            @mylogmsg("matrix type: $matrix_type")
            @mylogmsg("nev: $nev")

            if matrix_type == "sparse"
                psi_small_normed = normalize(psi_small)
                target_energy = real(dot(psi_small_normed, hamiltonian_rep * psi_small_normed))
                @mylogmsg("Target energy = $target_energy")

                # == iterative
                # @mylogmsg("Constructing shift invert Hamiltonian")
                # shift_invert_hamiltonian_rep = iterativeinvert(Shift(-target_energy, hamiltonian_rep))
                # @mylogmsg("Diagonalizing sparse Hamiltonian")
                # eigenvalues, eigenvectors = eigs(shift_invert_hamiltonian_rep; nev=nev, which=:LM, v0=psi_small_normed, tol=1E-6)

                # == factorization
                @mylogmsg("Constructing shift invert Hamiltonian")
                shift_invert_hamiltonian_rep = let
                    sparse_rep = sparse(hamiltonian_rep) - target_energy * I
                    return sparse_rep

                    f = lu(sparse_rep)
                    FactorizeInvert(f)
                end
                @mylogmsg("Diagonalizing sparse Hamiltonian")
                eigenvalues, eigenvectors = eigs(shift_invert_hamiltonian_rep; nev=nev, which=:LM, v0=psi_small_normed, tol=1E-6)

                # # # == scipy.sparse.linalg.eigsh
                # @mylogmsg("Constructing scipy sparse")
                # hamiltonian_rep_scipy = let s = sparse(hamiltonian_rep)
                #     sps.csc_matrix((s.nzval, s.rowval .- 1, s.colptr .- 1), shape=(s.m, s.n))
                # end
                # @mylogmsg("size: $(hamiltonian_rep_scipy.shape)")
                # @mylogmsg("Diagonalizing with scipy.sparse.linalg")
                # eigenvalues, eigenvectors = spsl.eigsh(
                #     hamiltonian_rep_scipy,
                #     k=nev,
                #     sigma=target_energy,
                #     which="LM",
                #     v0=psi_small_normed,
                #     tol=1E-6,
                #     return_eigenvectors=true
                # )

                # # == using Julia's eigs
                # sparse_hamiltonian_rep = sparse(hamiltonian_rep)
                # @mylogmsg("sparse diagonalization:")
                # eigenvalues, eigenvectors, nconv, niter, nmult, resid = eigs(sparse_hamiltonian_rep; nev=nev, which=:LM, v0=psi_small_normed, sigma=target_energy, tol=1E-6, ritzvec=true)
                # @mylogmsg("  nconv: $nconv")
                # @mylogmsg("  niter: $niter")
                # @mylogmsg("  nmult: $nmult")
                # @mylogmsg("  resid: $resid")

                eigenvalues, eigenvectors = reeigen(Hermitian(hamiltonian_rep), eigenvectors)
            else  # if matrix_type == "dense"
                @mylogmsg("Creating dense Hamiltonian matrix")
                hamiltonian_dense = Matrix(hamiltonian_rep)
                @mylogmsg("Diagonalizing dense Hamiltonian matrix")
                #eigenvalues, eigenvectors = spl.eigh(hamiltonian_dense, overwrite_a=true, driver="ev")
                eigenvalues, eigenvectors = eigen!(Hermitian(hamiltonian_dense))
            end

            @mylogmsg("Computing overlap")
            # Compute overlap
            neigen = length(eigenvalues)
            eigenoverlaps = Vector{ComplexF64}(undef, neigen)
            # @show size(eigenvectors)
            # @show dimension(rhssr)
            Threads.@threads for ieigen in 1:neigen
                eigenoverlaps[ieigen] = dot(eigenvectors[:, ieigen], psi_small)
            end


            let ol1 = sum(abs2.(psi_small)),
                ol2 = sum(abs2.(eigenoverlaps))
                @mylogmsg("sum(abs2.(psi_small)) = $(ol1)")
                @mylogmsg("sum(abs2.(overlaps)) = $(ol2)")
                @mylogmsg("leakage = $((ol1 - ol2) / ol1)")
            end

            parameter = OrderedDict(
                "shape"=>[shape[1,1], shape[2,1], shape[1,2], shape[2,2]],
                "Jx"=>Jx,
                "Jy"=>Jy,
                "Jz"=>Jz,
                "J1"=>J1,
                "J2"=>J2,
                "coloring"=>coloring,
            )
            space_symmetry_sector = OrderedDict(
                "tii" => tii,
                "pii" => pii,
                "pic" => pic,
                "point_symmetry" => symmetry(ssic.rest.symmetry).hermann_mauguin,
                "momentum" => (
                    lattice.unitcell.reducedreciprocallatticevectors *
                    fractional_momentum(ssymbed, ssic.normal.irrep_index)
                )
            )

            output_directory = dirname(output_filepath)
            if !isdir(output_directory)
                @mylogmsg("Creating directory $output_directory")
                mkpath(output_directory; mode=0o755)
            end

            @mylogmsg("Saving to $output_filepath")
            open(output_filepath, "w") do io
                ioc = XzCompressorStream(io)
                output_dict = OrderedDict(
                    "parameter" => parameter,
                    "space_symmetry_sector" => space_symmetry_sector,
                    "spinz" => Sz,
                    "tii" => tii,
                    "pii" => pii,
                    "pic" => pic,
                    "matrix_type" => matrix_type,
                    "eigenvalue" => eigenvalues,
                    "overlap_real" => real.(eigenoverlaps),
                    "overlap_imag" => imag.(eigenoverlaps),
                )
                MsgPack.pack(ioc, output_dict)
                close(ioc)
            end

        end
    end

    # space_symmetry_sectors = [
    #     OrderedDict(
    #         "tii" => ssic.normal.irrep_index,
    #         "pii" => ssic.rest.irrep_index,
    #         "pic" => ssic.rest.irrep_component,
    #         "point_symmetry" => symmetry(ssic.rest.symmetry).hermann_mauguin,
    #         "momentum" => (
    #             lattice.unitcell.reducedreciprocallatticevectors *
    #             fractional_momentum(ssymbed, ssic.normal.irrep_index)
    #         ),
    #     )
    #     for ssic in get_irrep_components(ssymbed)
    # ]


    @mylogmsg("Successfully finished")
end


function parse_commandline()
    s = ArgParseSettings()
    @add_arg_table! s begin
        "--shape"
            arg_type = Int
            nargs = 4
            required = true
        "--Jx"
            arg_type = Float64
            nargs = '*'
            default = [1.0]
            help = "values of Jx"
        "--Jy"
            arg_type = Float64
            nargs = '*'
            default = [1.0]
            help = "values of Jy"
        "--Jz"
            arg_type = Float64
            nargs = '+'
            help = "values of Jz"
        "--J1"
            arg_type = Float64
            default = 1.0
            help = "values of J1"
        "--J2"
            arg_type = Float64
            default = 0.0
            help = "values of J2"
        "--Sz"
            arg_type = Float64
            nargs = '*'
            help = "values of Sz to consider"
        "--max-dense"
            arg_type = Int64
            default = 20000
            help = "maximum hilbert space dimension to solve with dense matrix"
        "--nev"
            arg_type = Int
            default = 1000
            help = "number of eigenvalues to compute when using sparse"
        "--debug", "-d"
            help = "debug"
            action = :store_true
        "--coloring"
            arg_type = String
            required = true
            range_tester = x -> all(y->y in "123", x)
        "--force", "-f"
            help = "force run (overwrite)"
            action = :store_true
    end
    return parse_args(s)
end


# run in the background
function runbg(f)
    tid_main = Threads.threadid()
    simplelock = Threads.SpinLock()
    finished = Threads.Atomic{Bool}(false)
    Threads.@threads for i in 1:Threads.nthreads()
        if i != tid_main && trylock(simplelock) && !finished[]
            finished[] = true
            @async f()
            unlock(simplelock)
        end
    end
end

function main()
    parsed_args = parse_commandline()
    if parsed_args["debug"]
        logger = ConsoleLogger(stdout, Logging.Debug; meta_formatter=my_metafmt)
        global_logger(logger)
    else
        logger = ConsoleLogger(stdout, Logging.Info; meta_formatter=my_metafmt)
        global_logger(logger)
    end
    ss = parsed_args["shape"]
    J1 = parsed_args["J1"]
    J2 = parsed_args["J2"]

    Jxs = parsed_args["Jx"]
    Jys = parsed_args["Jy"]
    Jzs = parsed_args["Jz"]

    shape = [ss[1] ss[3]; ss[2] ss[4]]

    Sz = parsed_args["Sz"]

    max_dense = parsed_args["max-dense"]
    nev = parsed_args["nev"]

    force = parsed_args["force"]

    coloring = [parse(Int, x) for x in parsed_args["coloring"]]
    if length(coloring) != round(Int, det(shape)*3)
        @error "Length of coloring should match number of sites"
        exit(1)
    end

    #    tid_main = Threads.threadid()
    #    simplelock = Threads.SpinLock()
    #    finished = Threads.Atomic{Bool}(false)
    #    @mylogmsg("Number of threads: $(Threads.nthreads())")
    #    Threads.@threads for i in 1:Threads.nthreads()
    #        if i != tid_main
    #            if trylock(simplelock)
    #                if !finished[]
    #                    finished[] = true
    #                    @async begin
    #                        while true
    #                            GC.gc()
    #                            @mylogmsg("## logging memory usage")
    #                            sleep(60)
    #                        end
    #                    end
    #                end
    #                unlock(simplelock)
    #            end
    #        end
    #    end

    runbg() do
        while true
            GC.gc()
            @mylogmsg("## logging memory usage")
            sleep(60)
        end
    end

    for (Jx, Jy, Jz) in Iterators.product(Jxs, Jys, Jzs)
        t = Threads.@spawn compute_dynamics(
            Jx, Jy, Jz, J1, J2,
            shape, Sz,
            coloring,
            max_dense, nev,
            force,
        )
        fetch(t)
        GC.gc()
    end
end

#main()
s = compute_dynamics(
    1.0, 1.0, -0.5, 1.0, 0.0,
    [3 0; 0 3], Float64[-6.5],
    [1,2,3,1,2,3,1,2,3,1,2,3,1,2,3,1,2,3,1,2,3,1,2,3,1,2,3,],
    10, 1000,
    true,
)

getcolptr(S::SparseMatrixCSC)     = getfield(S, :colptr)
#getcolptr(S::SparseMatrixCSCView) = view(getcolptr(parent(S)), first(axes(S, 2)):(last(axes(S, 2)) + 1))
getrowval(S::SparseMatrixCSC) = rowvals(S)
#getrowval(S::SparseMatrixCSCView) = rowvals(parent(S))
getnzval( S::SparseMatrixCSC) = nonzeros(S)
#getnzval( S::SparseMatrixCSCView) = nonzeros(parent(S))
nzvalview(S::SparseMatrixCSC) = view(nonzeros(S), 1:nnz(S))
